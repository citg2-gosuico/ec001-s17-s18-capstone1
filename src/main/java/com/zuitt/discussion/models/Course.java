package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private double price;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    public Course(){}

    public Course(String name, String description, double price) {
        this.name = name;
        this.description = description;
        this.price = price;

    }

    public Long getId() {
        return id;
    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public User getUser(){
        return user;
    }
    public void setUser(User user){
        this.user=user;
    }

}